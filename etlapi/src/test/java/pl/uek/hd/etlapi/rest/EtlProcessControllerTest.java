package pl.uek.hd.etlapi.rest;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.uek.hd.etlapi.dto.ErrorCode;
import pl.uek.hd.etlcore.dto.EtlStatistics;
import pl.uek.hd.etlcore.service.EtlService;

import java.io.IOException;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(EtlProcessController.class)
@Ignore
public class EtlProcessControllerTest {

    @MockBean
    private EtlService etlService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldExtractReturnJsonWithStatistics() throws Exception {
        int deviceId = 123;
        given(this.etlService.performExtraction(deviceId)).
                willReturn(new EtlStatistics(10,0,0));

        mockMvc.perform(post("/etl/extract").param("id","123"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.extractedCount", is(10)))
                .andExpect(jsonPath("$.transformedCount", is(0)))
                .andExpect(jsonPath("$.loadedCount", is(0)));
    }

    @Test
    public void shouldExtractCaughtIllegalArgumentException() throws Exception {
        int deviceId = 123;

        given(this.etlService.performExtraction(deviceId)).
                willThrow(new IllegalArgumentException());

        mockMvc.perform(post("/etl/extract").param("id","123"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.message", is("Device with code: 123 does not exists")))
                .andExpect(jsonPath("$.errorCode", is(ErrorCode.ID_NOT_FOUD_ERR.toString())));
    }

    @Test
    public void shouldExtractCaughtIOException() throws Exception {
        int deviceId = 123;

        given(this.etlService.performExtraction(deviceId)).
                willThrow(new IOException());

        mockMvc.perform(post("/etl/extract").param("id","123"))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.message", is("Could not perform extract due to file read/write error")))
                .andExpect(jsonPath("$.errorCode", is(ErrorCode.IOEXCEPTION_ERR.toString())));
    }

    @Test
    public void shouldExtractCaughtIllegalStateException() throws Exception {
        int deviceId = 123;

        given(this.etlService.performExtraction(deviceId)).
                willThrow(new IllegalStateException());

        mockMvc.perform(post("/etl/extract").param("id","123"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.message", is("This operation is illegal now")))
                .andExpect(jsonPath("$.errorCode", is(ErrorCode.OPERATION_FORBIDDEN.toString())));
    }
}

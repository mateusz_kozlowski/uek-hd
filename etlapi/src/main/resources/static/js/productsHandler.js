$(document).ready(function () {
    var productsTable = new Vue({
        el: '#products-table',
        data: {
            devices: []
        },
        mounted: function () {
            var self = this;
            $.ajax({
                url: "http://localhost:8080/devices"
            }).then(function (response) {
                self.devices = response;
            });
        },
        updated: function () {
            console.log("Updating table!");
            if (!$.fn.dataTable.isDataTable('#dataTable')) {
                $('#dataTable').DataTable({});
            } else {
                $('#dataTable').DataTable().draw();
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                $.ajax({
                    url: "http://localhost:8080/devices"
                }).then(function (response) {
                    self.devices = response;
                });
            },
            removeDevice: function (deviceId) {
                var self = this;
                $.ajax({
                    url: "http://localhost:8080/devices/" + deviceId,
                    type: "DELETE"
                }).done(function (response) {
                    $('#dataTable').DataTable().rows("[data-deviceid='" + deviceId + "']").remove().draw();
                }).fail(function (response) {
                    window.alert("Problem occured");
                });
            }
        }
    });
})

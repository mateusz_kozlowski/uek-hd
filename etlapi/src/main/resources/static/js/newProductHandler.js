$(document).ready(function () {
    function downloadAndRedirect(id) {
        $.ajax({
            url: "http://localhost:8080/etl/etl/" + id,
            type: 'POST'
        }).done(function (response) {
            $("#addingSpinner").css("display", "none");
            window.location = "http://localhost:8080/products/" + id;
        }).fail(function (xhr, status, error) {
            $("#addingSpinner").css("display", "none");
            if (JSON.parse(xhr.responseText).errorCode === "ID_NOT_FOUD_ERR") {
                window.alert("No device with given id found on www.ceneo.pl");
            }
        });
    }

    $("#newProductButton").click(function () {
        $("#addingSpinner").css("display", "block");
        var id = $("#newproductId").val();
        if (!id) {
            $("#addingSpinner").css("display", "none");
            window.alert("Invalid id");
            return;
        }
        if (!$.isNumeric(id)) {
            $("#addingSpinner").css("display", "none");
            window.alert("Invalid id");
            return;
        }

        $.ajax({
            url: "http://localhost:8080/devices/" + id
        }).done(function (response) {
            window.location = "http://localhost:8080/products/" + id;
        }).fail(function (xhr, status, error) {
            console.log(status);
            if (error == "Not Found") {
                downloadAndRedirect(id);
            }
        });


    });
});

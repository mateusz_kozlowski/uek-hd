package pl.uek.hd;


import org.apache.commons.io.FileUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Main application class that loads the configuration, creates the beans and launches the server
 */
@SpringBootApplication
public class Application {

    /**
     * Main method
     * @param args Program arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            File[] directoriesToDelete = Paths.get(".").toFile().listFiles((dir, name) -> name.matches("\\d+"));
            Arrays.stream(directoriesToDelete).filter(File::isDirectory)
                    .forEach(directory -> {
                        try {
                            FileUtils.deleteDirectory(directory);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            Arrays.stream(beanNames).forEach(System.out::println);

        };
    }
}

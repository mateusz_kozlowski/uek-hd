package pl.uek.hd.etlapi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Spring controller, that handles HTTP requests and maps it to proper webpages.
 */
@Controller
@RequestMapping
public class WebpageController {

    /**
     * Handler that maps "/" path to the "Products" HTML page.
     * @param model Spring model object, used to render data on the HTML template.
     * @return name of the webpage template - "products"
     */
    @GetMapping
    public String mainPage(Model model) {
        return "products";
    }

    /**
     * Handler that maps "/products" path to the "Products" HTML page.
     * @param model Spring model object, used to render data on the HTML template.
     * @return name of the webpage template - "products"
     */
    @GetMapping("/products")
    public String mainPageProducts(Model model) {
        return "products";
    }

    /**
     * Handler that maps "/products/{id}" path to the "Product" HTML page. The page contains given product data.
     * @param model Spring model object, used to render data on the HTML template.
     * @param id id of the device
     * @return name of the webpage template - "product"
     */
    @GetMapping("/products/{id}")
    public String product(@PathVariable int id, Model model) {
        model.addAttribute("id", id);
        return "product";
    }

}

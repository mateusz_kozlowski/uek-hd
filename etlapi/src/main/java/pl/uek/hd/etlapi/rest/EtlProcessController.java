package pl.uek.hd.etlapi.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.uek.hd.etlapi.dto.ErrorResponseDto;
import pl.uek.hd.etlapi.dto.ErrorCode;
import pl.uek.hd.etlcore.constant.Operation;
import pl.uek.hd.etlcore.dto.EtlStatistics;
import pl.uek.hd.etlcore.service.EtlService;

import java.io.IOException;
import java.util.Set;

@RestController
@RequestMapping("/etl")
public class EtlProcessController {

    @Autowired
    EtlService etlService;

    @RequestMapping(value = "/extract/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> extract(@PathVariable(value = "id", required = true) int deviceId) {
        try {
            EtlStatistics statistics = etlService.performExtraction(deviceId);
            return new ResponseEntity<>(statistics, HttpStatus.OK);
        } catch (Exception e) {
            return handleException(e, deviceId);
        }
    }

    @RequestMapping(value = "/transform/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> transform(@PathVariable(value = "id", required = true) int deviceId) {
        try {
            EtlStatistics statistics = etlService.performTransform(deviceId);
            return new ResponseEntity<>(statistics, HttpStatus.OK);
        } catch (Exception e) {
            return handleException(e, deviceId);
        }
    }

    @RequestMapping(value = "/load/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> load(@PathVariable(value = "id", required = true) int deviceId) {
        try {
            EtlStatistics statistics = etlService.performLoad(deviceId);
            return new ResponseEntity<>(statistics, HttpStatus.OK);
        } catch (Exception e) {
            return handleException(e, deviceId);
        }
    }

    @RequestMapping(value = "/etl/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> etlProcess(@PathVariable(value = "id", required = true) int deviceId) {
        try {
            EtlStatistics statistics = etlService.performEtl(deviceId);
            return new ResponseEntity<>(statistics, HttpStatus.OK);
        } catch (Exception e) {
            return handleException(e, deviceId);
        }
    }

    @RequestMapping(value = "/operations/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> permittedOperations(@PathVariable(value = "id", required = true) int deviceId) {
        try{
            Set<Operation> avaibleOperations =  etlService.getAvailableOperationsForDevice(deviceId);
            return new ResponseEntity<>(avaibleOperations, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return badIdResponse(deviceId);
        }
    }

    private ResponseEntity<?> handleException(Exception e, int deviceId) {
        if(e instanceof IllegalArgumentException) {
            return badIdResponse(deviceId);
        } else if(e instanceof IOException) {
            return fileErrorResponse();
        } else if(e instanceof IllegalStateException) {
            return illegalOperationResponse();
        } else {
            return unknownErrorResponse();
        }
    }

    private ResponseEntity<?> badIdResponse(int deviceId) {
        ErrorResponseDto error = new ErrorResponseDto(
                String.format("Device with id %d not found", deviceId),
                ErrorCode.ID_NOT_FOUD_ERR);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<?> unknownErrorResponse() {
        ErrorResponseDto error = new ErrorResponseDto("", ErrorCode.UNKNOWN_ERR);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<?> illegalOperationResponse() {
        ErrorResponseDto error = new ErrorResponseDto(
                String.format("This operation is illegal now"),
                ErrorCode.OPERATION_FORBIDDEN);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<?> fileErrorResponse() {
        ErrorResponseDto error = new ErrorResponseDto(
                "Could not perform extract due to file read/write error",
                ErrorCode.IOEXCEPTION_ERR);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

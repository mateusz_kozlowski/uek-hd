package pl.uek.hd.etlapi.dto;

public enum ErrorCode {
    ID_NOT_FOUD_ERR,
    IOEXCEPTION_ERR,
    OPERATION_FORBIDDEN,
    UNKNOWN_ERR
}

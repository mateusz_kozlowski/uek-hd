package pl.uek.hd.etlapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class holding response data in case of error
 */
@Data
@AllArgsConstructor
public class ErrorResponseDto {
    /**
     * Message to be displayed on the GUI
     */
    private String message;

    /**
     * Unique code of error
     */
    private ErrorCode errorCode;

}

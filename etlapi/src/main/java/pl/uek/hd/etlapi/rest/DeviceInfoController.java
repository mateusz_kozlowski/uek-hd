package pl.uek.hd.etlapi.rest;

import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.uek.hd.etlcore.dto.FileResponse;
import pl.uek.hd.etlcore.dto.DeviceDto;
import pl.uek.hd.etlcore.dto.OpinionDto;
import pl.uek.hd.etlcore.service.DeviceService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class handling HTTP AJAX requests for devices data. Every response is in JSON format.
 * Base path is "/devices"
 */
@RestController
@RequestMapping("/devices")
public class DeviceInfoController {

    /**
     * Reference to device service layer, that handles business logic.
     */
    @Autowired
    private DeviceService deviceService;

    /**
     * REST handler, that handles "/devices" path with GET method and returns all devices with corresponding opinions
     * found in the database.
     * @return HTTP Response containing found devices in JSON format
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getDevices() {
        List<DeviceDto> devices = deviceService.getAllDevices();
        return ResponseEntity.status(HttpStatus.OK).body(devices);
    }

    /**
     * REST handler, that handles "/devices/{id}" path with GET method and returns device with given id, and corresponding opinions.
     * @param id id of the device
     * @return HTTP 200 Response containing found device in JSON format, HTTP 404 response with if device
     * was not found in the database
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getInfo(@PathVariable int id) {
        try {
            DeviceDto deviceDto = deviceService.findDevice(id);
            return ResponseEntity.status(HttpStatus.OK).body(deviceDto);
        } catch(IllegalArgumentException exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }

    /**
     * REST handler, that handles "/devices/{id}" path with DELETE method and deletes device with given id, with corresponding opinions.
     * @param id id of the device
     * @return HTTP 200 response in case of success, HTTP 404 response when device was not found
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDevice(@PathVariable int id) {
        try {
            deviceService.deleteDevice(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch(IllegalArgumentException exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
        }
    }

    /**
     * REST handler, that handles "/device/{id}/opinions path with DELETE method, that deletes opinions of device with given id.
     * The device itself is not deleted.
     * @param id id of the device
     * @return HTTP 200 response in case of success, HTTP 500 in case of error
     */
    @RequestMapping(value = "/{id}/opinions", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteOpinions(@PathVariable int id) {
        deviceService.clearAllOpinions(id);
        return ResponseEntity.ok().build();
    }

    /**
     * REST handler, that handles "/device/{id}/opinions/csv" path with GET method. It returns CSV file with data of requested device.
     * @param id id of the device
     * @return CSV file with opinions of given device, aggregated in the CSV file. HTTP 500 in case of error.
     */
    @RequestMapping(value = "{id}/opinions/csv", method = RequestMethod.GET, produces = "application/csv")
    public ResponseEntity<?> exportToCSV(@PathVariable int id) {
        FileResponse response = deviceService.generateCsv(id);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("content-disposition", "attachment; filename=\"" + response.getFilename() + ".csv\"");
        return ResponseEntity.ok()
                .contentLength(response.getContent().length)
                .headers(responseHeaders)
                .body(response.getContent());
    }

    /**
     * REST handler, that handles "/device/{id}/opinions/zip" path with GET method. It returns ZIP file with data of requested device.
     * @param id id of the device
     * @return ZIP file with device data and its opinions data, packed into single ZIP file. HTTP 500 in case of error.
     */
    @RequestMapping(value = "{id}/opinions/zip", method = RequestMethod.GET, produces = "application/zip")
    public ResponseEntity<?> exportAsZip(@PathVariable int id) {
        FileResponse response = deviceService.generateZip(id);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("content-disposition", "attachment; filename=\"" + response.getFilename() + ".zip\"");
        return ResponseEntity.ok()
                .contentLength(response.getContent().length)
                .headers(responseHeaders)
                .body(response.getContent());
    }
}

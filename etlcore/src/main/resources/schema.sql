CREATE TABLE Device (
    id INT PRIMARY KEY,
    type VARCHAR,
    brand VARCHAR,
    model VARCHAR,
    additional_info VARCHAR
);

CREATE TABLE Opinion (
    id INT auto_increment PRIMARY KEY,
    ceneo_id INT,
    device_id INT,
    drawbacks VARCHAR,
    advantages VARCHAR,
    summary VARCHAR,
    rating FLOAT,
    author VARCHAR DEFAULT 'Anonim',
    comment_date TIMESTAMP,
    recommended BOOLEAN,
    approved_count INT,
    disapproved_count INT,
    FOREIGN KEY (device_id) references Device(id)
);




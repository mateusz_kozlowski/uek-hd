package pl.uek.hd.etlcore.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Domain entity containing information about single device. It's constructors (no args and all args), setters and getters
 * are generated at compile time, so user can assume that they are present and set the fields properly
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"opinions"})
public class Device {

    /**
     * Id of the device, equal to Ceneo identifier
     */
    @Id
    private Integer id;

    /**
     * Type of the device
     */
    @Column
    private String type;

    /**
     * Brand of the device
     */
    @Column
    private String brand;

    /**
     * Model of the device
     */
    @Column
    private String model;

    /**
     * Additional information
     */
    @Column(name="additional_info")
    private String additionalInfo;

    /**
     * Set of opinions connected to the device
     */
    @OneToMany(mappedBy="device", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Opinion> opinions;

}

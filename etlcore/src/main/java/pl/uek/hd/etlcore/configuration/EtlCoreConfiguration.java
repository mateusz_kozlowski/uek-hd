package pl.uek.hd.etlcore.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.uek.hd.etlcore.etl.extraction.Extractor;
import pl.uek.hd.etlcore.etl.extraction.HtmlDownloader;
import pl.uek.hd.etlcore.etl.loading.Loader;
import pl.uek.hd.etlcore.etl.transformation.HtmlParser;
import pl.uek.hd.etlcore.etl.transformation.Transformer;
import pl.uek.hd.etlcore.util.FilesManager;

import javax.swing.text.html.HTML;
import java.io.IOException;
import java.nio.file.Paths;


@Configuration
@EnableJpaRepositories(basePackages = "pl.uek.hd.etlcore.repository")
@EntityScan(basePackages = "pl.uek.hd.etlcore.domain")
public class EtlCoreConfiguration {

    @Bean
    public HtmlDownloader buildHtmlDownloader() {
        return new HtmlDownloader("https://www.ceneo.pl/");
    }

    @Bean
    public FilesManager buildFilesManager() throws IOException {
        return new FilesManager(Paths.get(System.getProperty("user.dir")));
    }

}

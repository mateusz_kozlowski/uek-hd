package pl.uek.hd.etlcore.dto;

import lombok.Data;

/**
 * Simple object containing information required by file downloading functions of the project. It's constructors (no args and all args), setters and getters
 * are generated at compile time, so user can assume that they are present and set the fields properly
 */
@Data
public class FileResponse {

    /**
     * File content
     */
    private byte[] content;

    /**
     * File name
     */
    private String filename;

}

package pl.uek.hd.etlcore.util;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class handling path building
 */
public class PathBuilder {

    /**
     * Build extraction path for given device
     * @param deviceId Device id
     * @return Device extraction path - {deviceId}/extracted
     */
    public static Path buildExtractionPath(int deviceId) {
        return Paths.get(Integer.toString(deviceId),"extracted");
    }

    /**
     * Build transformation path for given device
     * @param deviceId Device id
     * @return Device transformation path - {deviceId}/transformed
     */
    public static Path buildTransformationPath(int deviceId) {
        return Paths.get(Integer.toString(deviceId),"transformed");
    }

    /**
     * Builds device path
     * @param deviceId Device id
     * @return Device path - {deviceId}
     */
    public static Path buildDevicePath(int deviceId) {
        return Paths.get(Integer.toString(deviceId));
    }
}

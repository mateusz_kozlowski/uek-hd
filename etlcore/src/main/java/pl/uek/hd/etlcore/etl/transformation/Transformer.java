package pl.uek.hd.etlcore.etl.transformation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.domain.Opinion;
import pl.uek.hd.etlcore.util.FilesManager;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Class responsible for performing transform operation. Transformation operation takes files from
 * {deviceId}/extracted directory and transforms them into JSON representations
 * of device and opinions.
 */
@Component
public class Transformer {

    /**
     * Files manager instance
     */
    private FilesManager filesManager;

    /**
     * Html parser instance
     */
    private HtmlParser htmlParser;

    /**
     * Constructor initializing class fields
     * @param htmlParser Html parses instance
     * @param filesManager Files manager instance
     */
    @Autowired
    public Transformer(HtmlParser htmlParser, FilesManager filesManager) {
        this.filesManager = filesManager;
        this.htmlParser = htmlParser;
    }

    /**
     * Transforms extracted device into domain JSON files. The files are stored in the {deviceId}/transformed directory.
     * "device" file contains device data, and each opinion has file name in "c{opinionId}" format (eg "c12345").
     * @param deviceId Id of the device to transform
     * @return Number of transformer opinions
     * @throws IOException When there was IO error
     * @throws IllegalStateException When there was no extraction performed before
     */
    public int transform(int deviceId) throws IOException {
        if(!filesManager.containsExtractedData(deviceId)) {
            throw new IllegalStateException("Device " + deviceId + " has to be extracted first!");
        }
        List<File> files = filesManager.listExtractedFiles(deviceId);
        int transformedCount = 0;
        files.stream().findFirst().ifPresent(f -> {
            try {
                Device device = htmlParser.parseDevice(f);
                filesManager.persistTransformedDevice(device, deviceId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        for(File f : files) {
            List<Opinion> opinions = htmlParser.parseComments(f);
            filesManager.persistOpinions(opinions, deviceId);
            transformedCount+= opinions.size();
        }
        filesManager.removeExtractionPath(deviceId);
        return transformedCount;
    }

}

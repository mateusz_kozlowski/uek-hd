package pl.uek.hd.etlcore.etl.transformation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.domain.Opinion;
import pl.uek.hd.etlcore.util.SemicolonTransformer;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class responsible for HTML files parsing
 */
@Component
public class HtmlParser {

    /**
     * SimpleDateFormat used to transform HTML string date into java timestamp object
     */
    public final SimpleDateFormat TIMESTAMP_FORMAT;

    /**
     * Semicolon encoder/decoder instance
     */
    private SemicolonTransformer semicolonTransformer;

    /**
     * Constructor initializing class fields. The date converter is hardcoded to "yyyy-MM-dd HH:mm:ss" format.
     * @param semicolonTransformer Instance of semicolon encoder/decoder
     */
    @Autowired
    public HtmlParser(SemicolonTransformer semicolonTransformer) {
        this.semicolonTransformer = semicolonTransformer;
        TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * Parses given HTML file
     * @param file File containg HTML content of the device
     * @return Device domain representation
     * @throws IOException When there was IO error
     */
    public Device parseDevice(File file) throws IOException {
        Document doc = Jsoup.parse(file, "UTF-8");
        Device deviceToReturn = new Device();
        deviceToReturn.setId(extractDeviceId(doc));
        deviceToReturn.setAdditionalInfo(extractDeviceAdditionalInfo(doc));
        deviceToReturn.setBrand(extractDeviceBrand(doc));
        deviceToReturn.setModel(extractDeviceModel(doc));
        deviceToReturn.setType(extractDeviceType(doc));
        return deviceToReturn;
    }

    /**
     * Extracts device type from the HTML file
     * @param doc Document containing HTML device content
     * @return Type as String
     */
    private String extractDeviceType(Document doc) {
        Element spanElement = doc.select("div nav dl dd span:last-child").last();
        return spanElement.html();
    }

    /**
     * Extracts model of the device
     * @param doc Document containing HTML device content
     * @return Model as String
     */
    private String extractDeviceModel(Document doc) {
        Elements modelTag = doc.select("tr[class=product-offer js_product-offer]");
        String modelValue = modelTag.attr("data-GAProductName");
        return modelValue.substring(modelValue.lastIndexOf("/")+1);
    }

    /**
     * Extracts brand of the device
     * @param doc Document containing HTML device content
     * @return Brand as String
     */
    private String extractDeviceBrand(Document doc) {
        Elements brandMeta = doc.select("meta[property=og:brand]");
        return brandMeta.attr("content");
    }

    /**
     * Extracts Device additional info
     * @param doc Document containing HTML device content
     * @return Additional info as String
     */
    private String extractDeviceAdditionalInfo(Document doc) {
        String additionalInfo = doc.getElementsByClass("ProductSublineTags").html();
        return additionalInfo;
    }

    /**
     * Extracts Device id
     * @param doc Document containing HTML device content
     * @return Device id as Integer
     */
    private Integer extractDeviceId(Document doc) {
        Elements url = doc.select("meta[property=og:url]");
        String urlString = url.attr("content");
        return Integer.parseInt(urlString.substring(urlString.lastIndexOf("/")+1));

    }

    /**
     * Extracts list of opinions from given file
     * @param file HTML file containing opinions
     * @return List of opinion domain objects
     * @throws IOException When there was IO error
     */
    public List<Opinion> parseComments(File file) throws IOException {
        Document doc = Jsoup.parse(file, "UTF-8");
        Elements reviews = doc.select("li[class=review-box js_product-review]");
        List<Opinion> opinions = reviews.stream().map(this::parseComment).collect(Collectors.toList());
        return opinions;
    }

    /**
     * Parses single opinion HTML element
     * @param comment HTML element containing opinion data
     * @return Opinion domain object
     */
    private Opinion parseComment(Element comment) {
        Opinion opinion = new Opinion();
        opinion.setCeneoId(extractCommentId(comment));
        opinion.setDrawbacks(extractCommentReview(comment, "cons"));
        opinion.setAdvantages(extractCommentReview(comment, "pros"));
        opinion.setSummary(extractCommentSummary(comment));
        opinion.setRating(extractCommentRating(comment));
        opinion.setAuthor(extractCommentAuthor(comment));
        try {
            opinion.setCommentDate(extractCommentDate(comment));
        } catch (ParseException e) {
            opinion.setCommentDate(null);
        }
        opinion.setRecommended(extractCommentRecommended(comment));
        opinion.setApprovedCount(extractCommentApprovedCount(comment));
        opinion.setDisapprovedCount(extractCommentDisapprovedCount(comment));
        return opinion;
    }

    /**
     * Extracts number of users that marked give opinion as unhelpful
     * @param comment HTML opinion element
     * @return Number of users as int
     */
    private int extractCommentDisapprovedCount(Element comment) {
        return Integer.parseInt(comment.select("button[class=vote-no js_product-review-vote js_vote-no] span").html());
    }

    /** Extracts number of users that marked give opinion as helpful
     * @param comment HTML opinion element
     * @return Number of users as int
     */
    private int extractCommentApprovedCount(Element comment) {
        return Integer.parseInt(comment.select("button[class=vote-yes js_product-review-vote js_vote-yes] span").html());
    }

    /**
     * Extracts information about product recommendation
     * @param comment HTML opinion element
     * @return True if user recommended product, false otherwise
     */
    private boolean extractCommentRecommended(Element comment) {
        return comment.select("div[class=reviewer-recommendation]").first() != null;
    }

    /**
     * Extract opinion date from the opinion
     * @param comment HTML opinion element
     * @return Opinion date as Timestamp
     * @throws ParseException When parsing string to timestamp was unsuccessful
     */
    private Timestamp extractCommentDate(Element comment) throws ParseException {
        String timestampString = comment.select("span[class=review-time] time").attr("datetime");
        return new Timestamp(TIMESTAMP_FORMAT.parse(timestampString).getTime());
    }

    /**
     * Extracts author data from opinion
     * @param comment HTML opinion element
     * @return Author as String
     */
    private String extractCommentAuthor(Element comment) {
        return comment.select("div[class=reviewer-name-line]").first().html();
    }

    /**
     * Extracts user rating from opinion
     * @param comment HTML opinion element
     * @return Rating as Double (1-5 scale, 0.5 step)
     */
    private Double extractCommentRating(Element comment) {
        String style = comment.select("span [class=score-marker score-marker--s]").attr("style");
        String width = style.split(" ")[1].replace("%;", "");
        return Double.parseDouble(width)/20.0;
    }

    /**
     * Extracts summary from option
     * @param comment HTML opinion element
     * @return Summary as String
     */
    private String extractCommentSummary(Element comment) {
        return comment.select("p[class=product-review-body]").first().html().replaceAll("<br>", "")
                .replaceAll("</br>", "")
                .replaceAll("<br/>", "");
    }

    /**
     * Extracts advantages/drawbacks from given opinion
     * @param comment HTML opinion element
     * @param type Type of data to parse ("pros" - advantages, "cons" - disadvantages)
     * @return String representation of the list, separated with semicolon, with user semicolons encoded
     */
    private String extractCommentReview(Element comment, String type) {
        Elements reviews = comment.select("div[class="+type+"-cell] ul li");
        if(reviews == null || reviews.isEmpty()) {
            return null;
        } else {
            return reviews.stream().map(e -> e.html())
                    .flatMap(e -> Arrays.stream(e.split(",")))
                    .map(String::trim)
                    .map(semicolonTransformer::encode).collect(Collectors.joining(";"));
        }
    }

    /**
     * Extracts comment id from the opinion
     * @param comment HTML opinion element
     * @return Opinion id as an integer
     */
    private Integer extractCommentId(Element comment) {
        Element buttonElement = comment.select("button[class=vote-yes js_product-review-vote js_vote-yes]").first();
        return Integer.parseInt(buttonElement.attr("data-review-id"));
    }
}

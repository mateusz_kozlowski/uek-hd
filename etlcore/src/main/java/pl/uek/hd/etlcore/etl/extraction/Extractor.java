package pl.uek.hd.etlcore.etl.extraction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.hd.etlcore.util.FilesManager;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Class providing html extraction functionality. Extraction operation
 * downloads HTML pages connected with the device and saves them in the {deviceId}/extracted directory.
 */
@Component
public class Extractor {

    /**
     * Object responsible for downloading html files
     */
    private HtmlDownloader htmlDownloader;

    /**
     * Object responsible for persisting downloaded files in the filesystem
     */
    private FilesManager filesManager;

    /**
     * Constructor initializing fields with given parameters
     * @param htmlDownloader HtmlDownloader instance
     * @param filesManager FilesManager instance
     */
    @Autowired
    public Extractor(HtmlDownloader htmlDownloader, FilesManager filesManager) {
        this.htmlDownloader = htmlDownloader;
        this.filesManager = filesManager;
    }

    /**
     * Method downloading and persisting device with given id in {deviceId}/extracted directory. Files are named
     * as ascending integers, where each is the page number that was downloaded.
     * @param deviceId Id of the device to perform html extraction on.
     * @return Number of html files downloaded
     * @throws IOException When IO error occured
     * @throws IllegalArgumentException When device with given id was not found on ceneo
     */
    public int extract(int deviceId) throws IOException {
        List<String> downloadedHtmls = htmlDownloader.downloadDeviceHtmlFiles(deviceId);
        filesManager.removeTransformationPath(deviceId);
        filesManager.removeExtractionPath(deviceId);
        filesManager.persistExtracted(downloadedHtmls, deviceId);
        return downloadedHtmls.size();
    }

}

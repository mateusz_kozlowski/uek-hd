package pl.uek.hd.etlcore.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * View representation of device object. It's constructors (no args and all args), setters and getters
 * are generated at compile time, so user can assume that they are present and set the fields properly
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeviceDto {

    /**
     * Database id of the device
     */
    int deviceId;

    /**
     * Device type
     */
    String deviceType;

    /**
     * Device brand
     */
    String deviceBrand;

    /**
     * Device model
     */
    String deviceModel;

    /**
     * Additional info
     */
    String deviceAdditionalInfo;

    /**
     * List of opinions, converted to its view models
     */
    List<OpinionDto> opinions;

}

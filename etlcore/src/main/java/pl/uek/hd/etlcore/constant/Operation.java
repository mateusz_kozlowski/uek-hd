package pl.uek.hd.etlcore.constant;

/**
 * Enum describing available ETL operations
 */
public enum Operation {
    /**
     * Extraction operation
     */
    EXTRACT,
    /**
     * Transform operation
     */
    TRANSFORM,
    /**
     * Load operation
     */
    LOAD
}

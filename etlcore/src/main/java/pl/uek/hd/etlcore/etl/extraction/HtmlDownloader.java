package pl.uek.hd.etlcore.etl.extraction;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Class used to download html files for specific device
 */
@Slf4j
public class HtmlDownloader {

    /**
     * Base of http requests
     */
    private String BASE_URL;

    /**
     * Constructor initializing the object with BASE_URL
     * @param BASE_URL Base url of the http requests (eg. "https://www.ceneo.pl/")
     */
    public HtmlDownloader(String BASE_URL) {
        this.BASE_URL = BASE_URL;
    }

    /**
     * Returns List of htmls for given device, each comment page is treated as a separate page
     * @param deviceId Device id
     * @return List of htmls
     * @throws IllegalArgumentException When error occured
     */
    public List<String> downloadDeviceHtmlFiles(int deviceId) throws IllegalArgumentException {
        try {
            return generateResults(deviceId);
        } catch (IOException e) {
            Throwable t = new IllegalArgumentException(e.getMessage());
            t.initCause(e);
            log.error("Error downloading html: ", t);
            throw (IllegalArgumentException) t;
        }
    }

    /**
     * Helper method performing HTTP requests for every page of the device and storing results as list of HTML strings
     * @param deviceId Device to download
     * @return List of HTMLS for given device
     * @throws IOException When there was error performing HTTP request
     */
    private List<String> generateResults(int deviceId) throws IOException {
        Document document = null;
        List<String> results = new ArrayList<>();
        int pageNumber = 1;
        do {
            document = Jsoup.connect(generateCurrentUrl(deviceId, pageNumber)).get();
            results.add(document.html());
            pageNumber++;
        } while(hasNextPage(document));
        return results;
    }

    /**
     * Helper method that generates HTTP path for given device and page
     * @param deviceId Device id
     * @param pageNumber Page number
     * @return Full path of the html page (eg. "https://www.ceneo.pl/47667343/opinie-2")
     */
    private String generateCurrentUrl(int deviceId, int pageNumber) {
        return String.format("%s%d/opinie-%d", BASE_URL, deviceId, pageNumber);
    }

    /**
     * Checks given HTML document if it has a hyperlink to next page
     * @param document HTML document
     * @return true if next page for device exists, false otherwise
     */
    private boolean hasNextPage(Document document) {
        Element pagination = document.getElementsByClass("pagination").first();
        if(pagination == null) {
            return false;
        }
        Stream<Element> liStream = pagination.getElementsByTag("li").stream();
        Optional<Element> foundElement = liStream.filter(el -> el.hasClass("arrow-next")).findAny();
        return foundElement.isPresent();
    }
}

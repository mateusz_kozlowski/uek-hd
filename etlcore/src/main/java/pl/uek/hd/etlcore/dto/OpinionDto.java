package pl.uek.hd.etlcore.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * View representation of opinion object. It's constructors (no args and all args), setters and getters
 * are generated at compile time, so user can assume that they are present and set the fields properly
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OpinionDto {

    /**
     * Database opinion id
     */
    private int opinionId;

    /**
     * Ceneo opinion id
     */
    private int ceneoId;

    /**
     * Collection of advantages provided by user
     */
    private Set<String> advantages;

    /**
     * Collection of drawbacks provided by user
     */
    private Set<String> drawbacks;

    /**
     * Summary of the opinion
     */
    private String summary;

    /**
     * Rating in scale 1-5, with 0.5 step
     */
    private double rating;

    /**
     * Author of the opinion
     */
    private String author;

    /**
     * Date of the opinion (in YYYY-MM-dd HH:mm:ss format)
     */
    private String date;

    /**
     * Does user recommend this product?
     */
    private boolean recommended;

    /**
     * Number of people that marked this opinion as helpful
     */
    private int approvedCount;

    /**
     * Number of people that marked this opinion as unhelpful
     */
    private int disapprovedCount;

}

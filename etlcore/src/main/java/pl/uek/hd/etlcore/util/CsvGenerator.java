package pl.uek.hd.etlcore.util;

import com.opencsv.CSVWriter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.domain.Opinion;
import pl.uek.hd.etlcore.dto.FileResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Class responsible foe preparing the CSV response
 */
@Component
public class CsvGenerator {

    /**
     * Semicolon encoder/decoder instance
     */
    private SemicolonTransformer semicolonTransformer;

    /**
     * Constructor initializing instance with semicolon encoder/decoder
     * @param semicolonTransformer Semicolon encoder/decoder
     */
    @Autowired
    public CsvGenerator(SemicolonTransformer semicolonTransformer) {
        this.semicolonTransformer = semicolonTransformer;
    }

    /**
     * Generates CSV file response. The CSV contains data about every opinion for given device, excluding the device itself.
     * The name contains device id and name.
     * @param device Device to generate CSV file from
     * @return Object holding CSV file content and name
     */
    public FileResponse generateCsv(Device device) {
        FileResponse response = new FileResponse();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CSVWriter writer = new CSVWriter(new OutputStreamWriter(outputStream));
        writer.writeNext(header());
        device.getOpinions().forEach(o -> writer.writeNext(opinion(o)));
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        response.setContent(outputStream.toByteArray());
        response.setFilename(generateFilename(device));
        return response;
    }

    /**
     * Generates CSV filename
     * @param device Device
     * @return Filename as String
     */
    private String generateFilename(Device device) {
        String filename = device.getId().toString() + "_";
        filename += device.getModel().replaceAll("\\s+", "_");
        return filename;
    }

    /**
     * Generates header of the CSV file (firstRow)
     * @return array containing names of every csv column
     */
    private String[] header() {
        return new String[] {"Database id", "Ceneo id", "Advantages", "Drawbacks", "Summary", "Rating", "Author", "Comment date", "Is recommended", "Approved", "Disapproved"};
    }

    /**
     * Converts given opinion into array of Strings, where every element is a column
     * @param opinion Opinion to transform
     * @return Array of opinion elements (id, ceneoId, advantages, drawbacks, summary, rating, author, date, isRecommended, approvedCount, disapprovedCount)
     */
    private String[] opinion(Opinion opinion) {
        String[] columns = new String[11];
        columns[0] = Integer.toString(opinion.getId());
        columns[1] = Integer.toString(opinion.getCeneoId());
        columns[2] = asString(opinion.getAdvantages());
        columns[3] = asString(opinion.getDrawbacks());
        columns[4] = opinion.getSummary();
        columns[5] = Double.toString(opinion.getRating());
        columns[6] = opinion.getAuthor();
        columns[7] = formattedTimestamp(opinion.getCommentDate());
        columns[8] = opinion.isRecommended() ? "YES" : "NO";
        columns[9] = Integer.toString(opinion.getApprovedCount());
        columns[10] = Integer.toString(opinion.getDisapprovedCount());
        return columns;
    }

    /**
     * Converts given timestamp to String representation
     * @param commentDate Date of opinion as Timestamp
     * @return String (YYYY-MM-dd HH:mm:ss) representation
     */
    private String formattedTimestamp(Timestamp commentDate) {
        Date date = new Date(commentDate.getTime());
        return new SimpleDateFormat("dd-MM-YYYY HH:mm:ss").format(date);
    }

    /**
     * Transforms given advantages/disadvantages string into CSV friendly representation
     * @param inputs Advantages/disadvantages
     * @return CSV friendly representation (removed colons)
     */
    private String asString(String inputs) {
        if(StringUtils.isBlank(inputs)) {
            return "";
        }
        return Arrays.stream(inputs.split(";"))
                .map(semicolonTransformer::decode)
                .reduce((s1,s2) -> s1 + System.lineSeparator() + s2).orElse("");
    }

}

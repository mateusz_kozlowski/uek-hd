package pl.uek.hd.etlcore.util;

import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Component;

@Component
public class SemicolonTransformer {

    public static final String separator = "%%%";

    public String encode(String s) {
        return s.replace(";", separator);
    }

    public String decode(String s) {
        return s.replace(separator, ";");
    }
}

package pl.uek.hd.etlcore.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pl.uek.hd.etlcore.dto.EtlStatistics;
import pl.uek.hd.etlcore.constant.Operation;

import java.io.IOException;
import java.util.Set;

/**
 * Service used to perform ETL operations
 */
@Component
public interface EtlService {

    /**
     * Performs full ETL process
     * @param deviceId Device identifier
     * @return Statistics regarding data processed during execution
     * @throws IllegalArgumentException When device with given id was not found
     * @throws IOException When there was IO error writing files or downloading HTMLS
     */
    EtlStatistics performEtl(int deviceId) throws IllegalArgumentException, IOException;

    /**
     * Performs extraction. Downloads HTMLS and saves them in the {deviceId}/extracted directory.
     * @param deviceId Device identifier
     * @return Statistics regarding data processed during execution
     * @throws IOException When could not save files on disk
     * @throws IllegalArgumentException When device with given id was not found
     */
    EtlStatistics performExtraction(int deviceId) throws IOException;

    /**
     * Performs transformation of extracted data. Results are saved in the {deviceId}/transformed directory.
     * @param deviceId Device identifier
     * @return Statistics regarding data processed during execution
     * @throws IllegalStateException When extraction wasn't performed prior to calling transform
     * @throws IOException When there was an IO error
     */
    EtlStatistics performTransform(int deviceId) throws IllegalStateException, IOException;

    /**
     * Loads files from {deviceId}/transformed to the database.
     * @param deviceId Device identifier (from external source)
     * @return Statistics concerning data processed during execution
     * @throws IllegalStateException When transformation wasn't performed prior to calling load
     * @throws IllegalArgumentException When device with given id was not found
     */
    EtlStatistics performLoad(int deviceId) throws IllegalStateException, IllegalArgumentException;

    /**
     * Checks available operations for given device. It bases on the filesystem ({deviceId} directory).
     * @param deviceId Device identifier
     * @return Set of available operations
     */
    Set<Operation> getAvailableOperationsForDevice(int deviceId) throws IllegalArgumentException;

}

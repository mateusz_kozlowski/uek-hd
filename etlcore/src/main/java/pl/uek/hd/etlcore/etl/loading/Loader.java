package pl.uek.hd.etlcore.etl.loading;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.domain.Opinion;
import pl.uek.hd.etlcore.repository.DeviceRepository;
import pl.uek.hd.etlcore.util.FilesManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Class responsible for loading data to the database.
 * Loading operation takes files from {deviceId}/transformed directory
 * and loads content to the database.
 */
@Component
public class Loader {

    /**
     * Instance of device repository object
     */
    private DeviceRepository repository;

    /**
     * Instajce of files manager object
     */
    private FilesManager filesManager;

    /**
     * Constructor initializing class fields
     * @param repository Repository instance
     * @param filesManager Files manager instance
     */
    @Autowired
    public Loader(DeviceRepository repository, FilesManager filesManager) {
        this.repository = repository;
        this.filesManager = filesManager;
    }

    /**
     * Method loading device with the given id into the database. Objects to load are taken from {deviceId}/transformer directory.
     * After successful load all device files are deleted.
     * @param deviceId Id of the device to perform load on
     * @return Number of opinions loaded into database
     * @throws IllegalStateException When loading wasn't precessed by the transformation operation
     */
    public int loadToDatabase(int deviceId) {
        Device loadedDevice = filesManager.loadTransformedDevice(deviceId);
        Device existingDevice = repository.findOne(deviceId);
        if(existingDevice == null) {
            repository.save(loadedDevice);
            filesManager.removeDevicePath(deviceId);
            return loadedDevice.getOpinions().size();
        } else {
            List<Opinion> opinionsToAdd = findOpinionsToAdd(loadedDevice.getOpinions(), existingDevice.getOpinions());
            existingDevice.getOpinions().addAll(opinionsToAdd);
            repository.save(existingDevice);
            filesManager.removeDevicePath(deviceId);
            return opinionsToAdd.size();
        }
    }

    /**
     * Helper method filtering the opinions and deciding which of them should be added into database. This operation
     * is required because id of the opinion is automaticaly generated, so upon ceneo opinion change we add new comment
     * with same ceneo id but different database id.
     * @param newOpinions List of opinions created by the transform operation
     * @param existingOpinions Opinions currently existing in the database
     * @return List of opinions that are new or updated
     */
    private List<Opinion> findOpinionsToAdd(Set<Opinion> newOpinions, Set<Opinion> existingOpinions) {
        List<Opinion> result = new ArrayList<>();
        for(Opinion opinion : newOpinions) {
            if(!existingOpinions.contains(opinion)) {
                result.add(opinion);
            }
        }
        return result;
    }

}

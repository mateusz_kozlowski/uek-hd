package pl.uek.hd.etlcore.util;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.dto.DeviceDto;
import pl.uek.hd.etlcore.dto.FileResponse;
import pl.uek.hd.etlcore.dto.OpinionDto;
import pl.uek.hd.etlcore.dto.converters.DeviceDtoConverter;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Class responsible for generating zip files
 */
@Component
public class ZipGenerator {

    /**
     * Domain model to view model converter instance
     */
    @Autowired
    private DeviceDtoConverter dtoConverter;

    /**
     * Generates response with zip content and file name. Zip contains every opinion as a separate file, and a device file,
     * which contains device data. Every file is in json format.
     * @param device Device to convert
     * @return Response with zip content and name
     */
    public FileResponse generateZip(Device device) {
        FileResponse response = new FileResponse();
        DeviceDto deviceDto = dtoConverter.getDeviceDto(device);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(baos);
        int counter = 0;
        try(ZipOutputStream zos = new ZipOutputStream(bufferedOutputStream)) {
            for(OpinionDto opinion : deviceDto.getOpinions()) {
                counter++;
                ZipEntry entry = new ZipEntry(generateName(counter, opinion));
                String asJson = new Gson().toJson(opinion);
                zos.putNextEntry(entry);
                zos.write(asJson.getBytes());
                zos.closeEntry();
            }
            ZipEntry zipEntry = new ZipEntry("device" + device.getId());
            String deviceJson = new Gson().toJson(deviceDto);
            zos.putNextEntry(zipEntry);
            zos.write(deviceJson.getBytes());
            zos.closeEntry();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
        response.setContent(baos.toByteArray());
        response.setFilename(device.getId() + "_" + device.getModel().replace(" ", "_"));
        return response;
    }

    /**
     * Generates name for opinion
     * @param counter Position of the opinion in the zio
     * @param opinion Opinion viewmodel
     * @return Name of the opinion file( {counter}id{ceneoId}_{date}
     */
    private String generateName(int counter, OpinionDto opinion) {
        return counter + "id" + opinion.getCeneoId() + "_" + opinion.getDate().replace(":", "")
                .replace(" ", "_")
                .replace("-", "");
    }
}

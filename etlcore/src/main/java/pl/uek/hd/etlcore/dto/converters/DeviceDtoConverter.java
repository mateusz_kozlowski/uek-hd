package pl.uek.hd.etlcore.dto.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.dto.DeviceDto;
import pl.uek.hd.etlcore.dto.OpinionDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Class transforming domain device objects into view representation
 */
@Component
public class DeviceDtoConverter  {

    /**
     * Reference to opinion transformer
     */
    private OpinionDtoConverter opinionDtoConverter;

    /**
     * Constructor which initializes the 'opinionDtoConverter' field
     * @param opinionDtoConverter Object transforming domain opinion objects into view representation
     */
    @Autowired
    DeviceDtoConverter(OpinionDtoConverter opinionDtoConverter) {
        this.opinionDtoConverter = opinionDtoConverter;
    }

    /**
     * Method returning view representation of given device object
     * @param device Device to be transformed to its view representation
     * @return View representation of the device
     */
    public DeviceDto getDeviceDto(Device device) {
        List<OpinionDto> opinions = new ArrayList<>();
        device.getOpinions().forEach(item -> opinions.add(opinionDtoConverter.getOpinionDto(item)));

        return new DeviceDto(device.getId(),
                             device.getType(),
                             device.getBrand(),
                             device.getModel(),
                             device.getAdditionalInfo(),
                             opinions);
    }
}

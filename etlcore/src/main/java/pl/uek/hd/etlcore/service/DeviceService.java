package pl.uek.hd.etlcore.service;

import pl.uek.hd.etlcore.dto.FileResponse;
import pl.uek.hd.etlcore.dto.DeviceDto;

import java.util.List;

/**
 * Bridge residing between database and API, used to perform actions connected with devices
 */
public interface DeviceService {

    /**
     * @return List of all devices found in database
     */
    List<DeviceDto> getAllDevices();

    /**
     * @param deviceId Id of required device
     * @return Device of given id
     */
    DeviceDto findDevice(int deviceId);

    /**
     * Clears opinions from given device
     * @param deviceId Identifier of device
     */
    void clearAllOpinions(int deviceId);

    /**
     * Generates CSV file for given device
     * @param deviceId Id of the device
     * @return Response containing file content and name
     * @throws IllegalArgumentException When device with given id was not found
     */
    FileResponse generateCsv(int deviceId);

    /**
     * Generate zip file containing json files with every opinion information
     * @param deviceId Id of the device
     * @return Response containing zip content and name
     */
    FileResponse generateZip(int deviceId);

    /**
     * Deletes device with given id from the database
     * @param id Id of the device
     */
    void deleteDevice(int id);
}

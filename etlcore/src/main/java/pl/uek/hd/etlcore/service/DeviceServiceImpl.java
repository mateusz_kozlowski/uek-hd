package pl.uek.hd.etlcore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.dto.FileResponse;
import pl.uek.hd.etlcore.dto.DeviceDto;
import pl.uek.hd.etlcore.dto.converters.DeviceDtoConverter;
import pl.uek.hd.etlcore.repository.DeviceRepository;
import pl.uek.hd.etlcore.util.CsvGenerator;
import pl.uek.hd.etlcore.util.ZipGenerator;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * The only implementation of the DeviceService interface
 */
@Transactional
@Service
public class DeviceServiceImpl implements DeviceService {

    /**
     * Device repository instance
     */
    @Autowired
    private DeviceRepository repository;

    /**
     * Domain to view converter instance
     */
    @Autowired
    private DeviceDtoConverter converter;

    /**
     * Csv generator instance
     */
    @Autowired
    private CsvGenerator csvGenerator;

    /**
     * Zip generator instance
     */
    @Autowired
    private ZipGenerator zipGenerator;

    @Override
    public List<DeviceDto> getAllDevices() {
        List<DeviceDto> foundDevices = new ArrayList<>();
        for(Device device : repository.findAll()) {
            DeviceDto deviceDto = converter.getDeviceDto(device);
            foundDevices.add(deviceDto);
        }
        return foundDevices;
    }

    @Override
    public DeviceDto findDevice(int deviceId) {
        Device device = repository.findOne(deviceId);
        if(device == null) {
            throw new IllegalArgumentException("No device with given id found");
        }
        return converter.getDeviceDto(device);
    }

    @Override
    public void clearAllOpinions(int deviceId) {
        Device device = repository.findOne(deviceId);
        if(device != null) {
            device.getOpinions().clear();
            repository.save(device);
        }
    }

    @Override
    public FileResponse generateCsv(int deviceId) {
        Device device = repository.findOne(deviceId);
        if(device != null) {
            return csvGenerator.generateCsv(device);
        } else {
            throw new IllegalArgumentException("Invalid id");
        }
    }

    @Override
    public FileResponse generateZip(int deviceId) {
        Device device = repository.findOne(deviceId);
        if(device != null) {
            return zipGenerator.generateZip(device);
        } else {
            throw new IllegalArgumentException("Invalid id");
        }
    }

    @Override
    public void deleteDevice(int id) {
        repository.delete(id);
    }

}

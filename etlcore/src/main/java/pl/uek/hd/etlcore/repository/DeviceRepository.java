package pl.uek.hd.etlcore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.uek.hd.etlcore.domain.Device;

/**
 * Basic device CRUD repository interface, that is used by Spring to generate proper queries.
 */
@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {
}

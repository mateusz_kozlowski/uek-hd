package pl.uek.hd.etlcore.dto.converters;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pl.uek.hd.etlcore.domain.Opinion;
import pl.uek.hd.etlcore.dto.OpinionDto;
import pl.uek.hd.etlcore.util.SemicolonTransformer;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class transforming domain opinion objects into view representation
 */
@Component
public class OpinionDtoConverter {

    /**
     * Object used to encode and decode semicolons present in user advantages and drawbacks
     */
    private SemicolonTransformer semicolonTransformer;

    /**
     * SimpleDateFormat object used to format dates and timestamps to string representation
     */
    private final SimpleDateFormat sdf;

    /**
     * Constructor initializing fields
     * @param semicolonTransformer Semicolon transformer to be used
     */
    @Autowired
    public OpinionDtoConverter(SemicolonTransformer semicolonTransformer) {
        this.semicolonTransformer = semicolonTransformer;
        this.sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    }

    /**
     * Method converting domain opinion objects into it's view representation
     * @param opinion Opinion to be converted
     * @return View representation of given opinion
     */
    public OpinionDto getOpinionDto(Opinion opinion) {
        Set<String> advantages = asSet(opinion.getAdvantages());
        Set<String> drawbacks = asSet(opinion.getDrawbacks());

        return new OpinionDto(opinion.getId(),
                              opinion.getCeneoId(),
                              advantages,
                              drawbacks,
                              opinion.getSummary(),
                              opinion.getRating(),
                              opinion.getAuthor(),
                              sdf.format(new Date(opinion.getCommentDate().getTime())),
                              opinion.isRecommended(),
                              opinion.getApprovedCount(),
                              opinion.getDisapprovedCount());
    }

    /**
     * Helper method transforming domain string representation of advantages and drawbacks to Set.
     * @param stringRepresentation String (domain) representation of drawbacks or advantages
     * @return Empty Set when input is blank, otherwise Set representation of the advantages/drawbacks.
     */
    private Set<String> asSet(String stringRepresentation) {
        if(StringUtils.isBlank(stringRepresentation)) {
            return new HashSet<>();
        } else {
            List<String> advantages = Arrays.asList(stringRepresentation.split(";"));
            return advantages.stream().map(semicolonTransformer::decode).collect(Collectors.toSet());
        }
    }

}

package pl.uek.hd.etlcore.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.hd.etlcore.constant.Operation;
import pl.uek.hd.etlcore.dto.EtlStatistics;
import pl.uek.hd.etlcore.etl.extraction.Extractor;
import pl.uek.hd.etlcore.etl.loading.Loader;
import pl.uek.hd.etlcore.etl.transformation.Transformer;
import pl.uek.hd.etlcore.util.PathBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * The only implementation of EtlService interface
 */
@AllArgsConstructor
@Slf4j
@Component
public class EtlServiceImpl implements EtlService {

    /**
     * Extractor instance
     */
    @Autowired
    private Extractor extractor;

    /**
     * Transformer instance
     */
    @Autowired
    private Transformer transformer;

    /**
     * Loader instance
     */
    @Autowired
    private Loader loader;

    @Override
    public EtlStatistics performEtl(int deviceId) throws IllegalArgumentException, IOException {
        int extractedFilesCount = extractor.extract(deviceId);
        int transformedFilesCount = transformer.transform(deviceId);
        int loadedOpinionsCount = loader.loadToDatabase(deviceId);
        return EtlStatistics.builder().extractedCount(extractedFilesCount)
                .transformedCount(transformedFilesCount)
                .loadedCount(loadedOpinionsCount)
                .build();
    }

    @Override
    public EtlStatistics performExtraction(int deviceId) throws IOException {
        int extractedFilesCount = extractor.extract(deviceId);
        return EtlStatistics.builder().extractedCount(extractedFilesCount).build();
    }

    @Override
    public EtlStatistics performTransform(int deviceId) throws IllegalStateException, IOException {
        int transformedFilesCount = transformer.transform(deviceId);
        return EtlStatistics.builder().transformedCount(transformedFilesCount).build();
    }

    @Override
    public EtlStatistics performLoad(int deviceId) throws IllegalStateException {
        int loadedOpinionsCount = loader.loadToDatabase(deviceId);
        return EtlStatistics.builder().loadedCount(loadedOpinionsCount).build();
    }

    @Override
    public Set<Operation> getAvailableOperationsForDevice(int deviceId) {
        Path extractionPath = PathBuilder.buildExtractionPath(deviceId);
        Path transformationPath = PathBuilder.buildTransformationPath(deviceId);
        Set<Operation> availableOperations = new LinkedHashSet<>();
        availableOperations.add(Operation.EXTRACT);

        if(Files.exists(extractionPath) && extractionPath.toFile().listFiles().length != 0) {
            availableOperations.add(Operation.TRANSFORM);
        } else if(Files.exists(transformationPath) && transformationPath.toFile().listFiles().length != 0) {
            availableOperations.add(Operation.LOAD);
        }

        return availableOperations;
    }
}

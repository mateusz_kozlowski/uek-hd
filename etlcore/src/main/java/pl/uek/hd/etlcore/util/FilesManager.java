package pl.uek.hd.etlcore.util;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import pl.uek.hd.etlcore.domain.Device;
import pl.uek.hd.etlcore.domain.Opinion;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class responsible for handling file writing/reading
 */
public class FilesManager {

    /**
     * Base path for resolving every path
     */
    private Path basePath;

    /**
     * Constructor initializing object with the base path
     * @param basePath Base path
     */
    public FilesManager(Path basePath) {
        this.basePath = basePath;
    }

    /**
     * Persists given list of strings as extracted files (in the extracted path)
     * @param data List of contents to persist
     * @param deviceId Device id
     * @throws IOException when there was an IO error
     */
    public void persistExtracted(List<String> data, int deviceId) throws IOException {
        Path extractionPath = basePath.resolve(PathBuilder.buildExtractionPath(deviceId));
        if(!Files.exists(extractionPath)) {
            Files.createDirectories(extractionPath);
        }
        persist(data, extractionPath);
    }

    /**
     * Persists given list of strings as transformed files (in the transformed path)
     * @param data List of contents to persist
     * @param deviceId Device id
     * @throws IOException When there was an IO error
     */
    public void persistTransformed(List<String> data, int deviceId) throws IOException {
        Path transformationPath = basePath.resolve(PathBuilder.buildTransformationPath(deviceId));
        if(!Files.exists(transformationPath)) {
            Files.createDirectories(transformationPath);
        }
        persist(data, transformationPath);
    }

    /**
     * Persists given list of transformed opinions (in the transformed path), as JSONs
     * @param opinions List of opinions to persist
     * @param deviceId Device id
     * @throws IOException When there was an IO error
     */
    public void persistOpinions(List<Opinion>opinions, int deviceId) throws IOException {
        Path transformationPath = basePath.resolve(PathBuilder.buildTransformationPath(deviceId));
        if(!Files.exists(transformationPath)) {
            Files.createDirectories(transformationPath);
        }
        opinions.stream().forEach(o -> {
            try {
                Files.write(Paths.get(transformationPath.toString(), "c"+o.getCeneoId()), new Gson().toJson(o).getBytes("UTF-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Returns files extracted for given device id
     * @param deviceId Device id
     * @return List of found extracted files for given device
     */
    public List<File> listExtractedFiles(int deviceId) {
        Path extractionPath = basePath.resolve(PathBuilder.buildExtractionPath(deviceId));
        return Lists.newArrayList(extractionPath.toFile().listFiles());
    }

    /**
     * Helper method, that persists given strings on the given path (filenames are successful integers)
     * @param data List of strings to save
     * @param path Path to save strings on
     * @throws IOException When there was an IO error
     */
    private void persist(List<String> data, Path path) throws IOException {
        for(int i = 0; i<data.size(); i++) {
            String stringToPersist = data.get(i);
            File file = path.resolve(Integer.toString(i)).toFile();
            persistStringAs(stringToPersist, file);
        }
    }

    /**
     * Persists given String as a specific file
     * @param data String to save
     * @param file File location
     * @throws IOException When there was an IO error
     */
    private void persistStringAs(String data, File file) throws IOException {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(data);
        }
    }

    /**
     * Checks if given device has any extracted data in the extraction directory
     * @param deviceId Device id
     * @return true if {deviceId}/extracted exists and has files, false otherwise
     */
    public boolean containsExtractedData(int deviceId) {
        Path extractionPath = basePath.resolve(PathBuilder.buildExtractionPath(deviceId));
        if(!extractionPath.toFile().exists()) {
            return false;
        }
        return extractionPath.toFile().listFiles().length > 0;
    }

    /**
     * Checks if given device has any transformed data in the transformation directory
     * @param deviceId Device id
     * @return true if {deviceId}/transformed exists and has files, false otherwise
     */
    public boolean containsTransformedData(int deviceId) {
        Path transformationPath = basePath.resolve(PathBuilder.buildTransformationPath(deviceId));
        if(!transformationPath.toFile().exists()) {
            return false;
        }
        return transformationPath.toFile().listFiles().length > 0;
    }

    /**
     * Persists device (as JSON) in the {deviceId}/transformed/device file
     * @param device Device
     * @param deviceId DeviceId
     * @throws IOException When there was an IO error
     */
    public void persistTransformedDevice(Device device, int deviceId) throws IOException {
        Path transformationPath = basePath.resolve(PathBuilder.buildTransformationPath(deviceId));
        if(!Files.exists(transformationPath)) {
            Files.createDirectories(transformationPath);
        }
        Files.write(transformationPath.resolve("device"), new Gson().toJson(device).getBytes("UTF-8"));
    }

    /**
     * Loads device from the {deviceId}/transformed/device JSON file
     * @param deviceId DeviceId
     * @return Device domain object
     */
    public Device loadTransformedDevice(int deviceId) {
        Path transformationPath = basePath.resolve(PathBuilder.buildTransformationPath(deviceId));
        if(!transformationPath.toFile().exists()) {
            throw new IllegalStateException("No transformed data for device with id " + deviceId);
        }
        if(transformationPath.toFile().listFiles().length == 0) {
            throw new IllegalStateException("No transformed data for device with id " + deviceId);
        }
        Set<Opinion> opinions = loadTransformedOpinions(transformationPath);
        File deviceFile = transformationPath.resolve("device").toFile();
        Device device;
        try(InputStream is = new FileInputStream(deviceFile)) {
            String deviceContent = IOUtils.readLines(is, StandardCharsets.UTF_8).stream()
                    .reduce((s1, s2) -> s1 + s2).get();
            device = new Gson().fromJson(deviceContent, Device.class);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        opinions.forEach(o -> o.setDevice(device));
        device.setOpinions(opinions);
        return device;
    }

    /**
     * Removes the {deviceId}/transformed path
     * @param deviceId Device id
     */
    public void removeTransformationPath(int deviceId) {
        Path transformationPath = PathBuilder.buildTransformationPath(deviceId);
        removeFiles(transformationPath);
    }

    /**
     * Removes the {deviceId}/extracted path
     * @param deviceId Device id
     */
    public void removeExtractionPath(int deviceId) {
        Path extractionPath = PathBuilder.buildExtractionPath(deviceId);
        removeFiles(extractionPath);
    }

    /**
     * Removes the {deviceId}/transformed path and {deviceId}/extracted path
     * @param deviceId Device id
     */
    public void removeDevicePath(int deviceId) {
        removeTransformationPath(deviceId);
        removeExtractionPath(deviceId);
        Path devicePath = PathBuilder.buildDevicePath(deviceId);
        removeFiles(devicePath);
    }

    /**
     * Loads opinions from every opinion JSON file located in given path
     * @param path Path to load opinion from
     * @return Set of loaded opinion domain objects
     */
    private Set<Opinion> loadTransformedOpinions(Path path) {
        try {
            File[] opinionsList = path.toFile().listFiles((dir, name) -> name.startsWith("c"));

            Set<Opinion> opinions = new HashSet<>();
            for(File file : opinionsList) {
                try {
                    FileReader reader = new FileReader(file);
                    Opinion opinion = new Gson().fromJson(reader, Opinion.class);
                    opinions.add(opinion);
                    reader.close();
                } finally {

                }
            }
            return opinions;
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    /**
     * Removes given directory/file
     * @param path Path to remove
     */
    private void removeFiles(Path path)   {
        try {
            FileUtils.deleteDirectory(path.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

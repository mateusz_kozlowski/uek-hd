package pl.uek.hd.etlcore.util;

import static org.assertj.core.util.Files.newFolder;
import static org.junit.Assert.*;

import org.assertj.core.util.Strings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class FilesManagerTest {

    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    private Path basePath;

    private List<String> testStringList;

    @Before
    public void setupTest() throws IOException {
        basePath = temp.newFolder().toPath();
        testStringList = IntStream.range(0, 3).mapToObj(Integer::toString).map(s -> Strings.join(s, "xxx").toString()).collect(Collectors.toList());
    }

    @Test
    public void checkIfFilesManagerPersistedFiles() throws IOException {
        FilesManager fm = new FilesManager(basePath);
        fm.persistExtracted(testStringList, 1);
        fm.persistTransformed(testStringList, 1);
        int it = 0;
        for(String file : testStringList) {
            checkIfFileExists(Paths.get(basePath.toString(), Integer.toString(1), "extracted", Integer.toString(it)).toString());
            checkIfFileExists(Paths.get(basePath.toString(), Integer.toString(1), "transformed", Integer.toString(it)).toString());
            it++;
        }
    }

    @Test
    public void checkIfManagerCorrectlyChecksTransformedData() throws IOException {
        FilesManager fm = new FilesManager(basePath);
        assertFalse(fm.containsExtractedData(1));
        assertFalse(fm.containsTransformedData(1));
        fm.persistExtracted(testStringList, 1);
        assertTrue(fm.containsExtractedData(1));
        fm.persistTransformed(testStringList, 1);
        assertTrue(fm.containsTransformedData(1));
    }

    private void checkIfFileExists(String filename) {
        boolean exists = Files.exists(basePath.resolve(filename));
        assertTrue(exists);
    }
}

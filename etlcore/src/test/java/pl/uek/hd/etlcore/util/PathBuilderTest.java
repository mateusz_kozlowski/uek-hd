package pl.uek.hd.etlcore.util;

import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.*;

public class PathBuilderTest {

    @Test
    public void shouldPathBuilderReturnCorrectExtractionPath() {
        assertEquals(PathBuilder.buildExtractionPath(5), Paths.get("5", "extracted"));
    }

    @Test
    public void shouldPathBuilderReturnCorrectTransformationPath() {
        assertEquals(PathBuilder.buildTransformationPath(5), Paths.get("5", "transformed"));
    }

}

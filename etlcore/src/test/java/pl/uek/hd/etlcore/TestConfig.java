package pl.uek.hd.etlcore;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"pl.uek.hd.etlcore.dto.converters"})
public class TestConfig {
}

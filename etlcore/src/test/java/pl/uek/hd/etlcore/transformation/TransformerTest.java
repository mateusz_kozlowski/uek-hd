package pl.uek.hd.etlcore.transformation;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import pl.uek.hd.etlcore.etl.transformation.HtmlParser;
import pl.uek.hd.etlcore.etl.transformation.Transformer;
import pl.uek.hd.etlcore.util.FilesManager;
import pl.uek.hd.etlcore.util.SemicolonTransformer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Ignore
public class TransformerTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private Path basePath;

    @Before
    public void setupTestDirectory() throws IOException {
        basePath = temporaryFolder.newFolder().toPath();
    }
    @Test
    public void shouldTransformerThrowExceptionWhenNoExtractionWasExecuted() throws IOException {
        Transformer transformer = new Transformer(new HtmlParser(new SemicolonTransformer()), new FilesManager(basePath));
        boolean exceptionThrown = false;
        try {
            transformer.transform(100);
        } catch(IllegalStateException ex) {
            exceptionThrown = true;
        }
        assertTrue(exceptionThrown);
    }

    @Test
    public void shouldTransformerReturnCorrectNumberOfTransformedComments() throws IOException {
        Transformer transformer = getTransformer();
        assertEquals(2, transformer.transform(100));
    }

    @Test
    public void shouldTransformerPersistCommentsData() throws IOException {
        Transformer transformer = getTransformer();
        assertEquals(2, transformer.transform(100));
        File expectedFile1 = Paths.get(basePath.toString(), "100", "transformed", "c4257125").toFile();
        File expectedFile2 = Paths.get(basePath.toString(), "100", "transformed", "c3967924").toFile();
        assertTrue(expectedFile1.exists());
        assertTrue(expectedFile2.exists());
    }

    @Test
    public void shouldTransformerPersistDevice() throws IOException {
        Transformer transformer = getTransformer();
        assertEquals(2, transformer.transform(100));
        File expectedFile1 = Paths.get(basePath.toString(), "100", "transformed", "device").toFile();
        assertTrue(expectedFile1.exists());
    }

    private Transformer getTransformer() throws IOException {
        Transformer transformer = new Transformer(new HtmlParser(new SemicolonTransformer()), new FilesManager(basePath));
        Path extractedDirectory = Files.createDirectories(basePath.resolve(Paths.get("100", "extracted")));
        byte[] file1 = Files.readAllBytes(Paths.get("src/test/resources/test_html1.html"));
        byte[] file2 = Files.readAllBytes(Paths.get("src/test/resources/test_html2.html"));
        Files.write(Paths.get(extractedDirectory.toString(), "1"), file1);
        Files.write(Paths.get(extractedDirectory.toString(), "2"), file2);
        return transformer;
    }

}

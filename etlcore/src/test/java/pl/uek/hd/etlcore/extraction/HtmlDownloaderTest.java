package pl.uek.hd.etlcore.extraction;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import pl.uek.hd.etlcore.etl.extraction.HtmlDownloader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
public class HtmlDownloaderTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(8089));

    public final static String BASE_URL = "http://localhost:8089/";

    public final static int nonExistingId = 666;

    public final static int existingId = 44923177;

    private String body1;

    private String body2;

    @Test
    public void shouldHtmlDownloaderReturnProperListOfStrings() throws IOException {
        initHtmlDownloadTest();
        HtmlDownloader htmlDownloader = new HtmlDownloader(BASE_URL);
        List<String> receivedList = htmlDownloader.downloadDeviceHtmlFiles(existingId);
        assertEquals(2, receivedList.size());
    }

    private String removeWhitespaces(String input) {
        return input.replace("\\s+", "");
    }

    private void initHtmlDownloadTest() throws IOException {
        body1 = new String(Files.readAllBytes(Paths.get("src/test/resources/test_html1.html")));
        body2 = new String(Files.readAllBytes(Paths.get("src/test/resources/test_html2.html")));
        stubFor(get(urlEqualTo("/"+ HtmlDownloaderTest.existingId+"/opinie-1"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/xml")
                        .withBody(body1)));

        stubFor(get(urlEqualTo("/"+HtmlDownloaderTest.existingId+"/opinie-2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/xml")
                        .withBody(body2)));
    }

    @Test
    public void shouldHtmlDownloaderThrowExceptionOn404() {
        init404Test();
        HtmlDownloader htmlDownloader = new HtmlDownloader(BASE_URL);
        boolean thrownException = false;
        try {
            htmlDownloader.downloadDeviceHtmlFiles(nonExistingId);
        } catch(IllegalArgumentException ex) {
            thrownException = true;
        }
        assertTrue(thrownException);
    }

    private void init404Test() {
        stubFor(get(urlEqualTo("/" + HtmlDownloaderTest.nonExistingId+"/opinie-1"))
                //.withHeader("Accept", equalTo("text/html"))
                .willReturn(aResponse()
                        .withStatus(404)));
    }

}
